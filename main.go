package main

import (
	"flag"
	"fmt"
	"gitlab.com/celinelevy/cyoa/cyoa"
	"log"
	"net/http"
	"os"
	"text/template"
)

var htmlTemplate = `
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Choose your own adventure</title>
</head>
<body>
<h1>{{.Title}}</h1>
{{range .Story}}
{{.}}
{{end}}
{{range .Options}}
<a href="/{{.Arc}}">{{.Text}}</a>
{{end}}
<script src="js/scripts.js"></script>
</body>
</html>
	`

func NewHandler(s cyoa.Story) http.Handler {
	return handler{s}
}

type handler struct {
	s cyoa.Story
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	tpl := template.Must(template.New("").Parse(htmlTemplate))
	err := tpl.Execute(w, h.s["intro"])
	if err != nil {
		panic(err)
	}
}


func main()  {
	port := flag.Int("port", 3000, "CYOA web app")
	file := flag.String("file","stories.json","JSON file with stories")
	flag.Parse()
	fmt.Printf("Using stories in %s.\n", *file)

	f, err := os.Open(*file)
	if err != nil {
		panic(err)
	}
	var story cyoa.Story
	 story, err = cyoa.JsonStory(f)
	 if err != nil {
		panic(err)
	}
	fmt.Printf("%+v",story)

	 h := NewHandler(story)
	 log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), h))
}
